##### My notes from the Pluralsight [Getting Started with React video tutorial](https://app.pluralsight.com/library/courses/react-js-getting-started/table-of-contents) by [Samer Buna](https://twitter.com/samerbuna) from [jscomplete.com](https://jscomplete.com)

#### Table of Contents:
##### Notes from videos
1. [First component](1-your-first-component.md):
2. [Resusable components](2-reusable-components.md)
3. [Working with data from an API and user input](3-work-w-data.md)
##### Playing with the ideas
4. [Increment add with user input](3b-dynamic-add.js)
5. [Live Search](5-live-search.js) (refactored from https://codepen.io/evg_/pen/LRLVdY/ based on syntax and structure from tutorial) (works on jscomplete.com/repl but not codepen.io (??!!))
