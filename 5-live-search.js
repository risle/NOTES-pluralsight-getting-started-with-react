const CONTACTS = [
  {
    id: 1,
    name: "Bat Man",
    phoneNumber: "509-586-6687",
    image: "https://s3.amazonaws.com/uifaces/faces/twitter/jsa/128.jpg"
  },
  {
    id: 2,
    name: "Clark Kent",
    phoneNumber: "208-644-4233",
    image: "https://s3.amazonaws.com/uifaces/faces/twitter/idiot/128.jpg"
  },
  {
    id: 3,
    name: "Wonder Woman",
    phoneNumber: "244-462-9889",
    image: "https://s3.amazonaws.com/uifaces/faces/twitter/vista/128.jpg"
  },
  {
    id: 4,
    name: "Spider Man",
    phoneNumber: "306-949-5312",
    image: "https://s3.amazonaws.com/uifaces/faces/twitter/c_southam/128.jpg"
  }
];

const Contact = (props) => {
    return (
      <li className="contact">
        <img
          src={props.image}
          width="128px"
          height="128px"
          className="contact-image"
        />
        <div className="contact-info">
          <div className="contact-name">{props.name}</div>
          <div className="contact-number">{props.phoneNumber}</div>
        </div>
      </li>
    );

};

class ContactsList extends React.Component {
  state = { displayedContacts: CONTACTS };
  handleSearch = (e) => {
    let searchQuery = e.target.value.toLowerCase();
    let displayedContacts = CONTACTS.filter((person) => {
      let searchValue = person.name.toLowerCase();
      return searchValue.indexOf(searchQuery) !== -1;
    });

    this.setState({
      displayedContacts
    });
  }
  render() {
    return (
      <div className="contacts">
        <input
          type="text"
          className="search-field"
          onChange={this.handleSearch}
        />
        <ul className="contacts-list">
          {this.state.displayedContacts.map(function(el) {
            return (
              <Contact
                key={el.id}
                name={el.name}
                phoneNumber={el.phoneNumber}
                image={el.image}
              />
            );
          })}
        </ul>
      </div>
    );
  }
};

ReactDOM.render(<ContactsList />, document.getElementById(app));
