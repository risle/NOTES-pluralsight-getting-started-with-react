#### Notes on: Getting Started with React
#### [Pluralsight with Samer Buna](https://app.pluralsight.com/player?course=react-js-getting-started&author=samer-buna&name=react-js-getting-started-m1&clip=0&mode=live)
# 3: Working with Data

[starting code](bit.ly/psreact3)

[playground](https://jscomplete.com/repl)

```javascript
const Card = (props) => {
	return (
  	<div style={{margin: '1em'}}>
    	<img width="75" src={props.avatar_url} />
      <div style={{display: 'inline-block', marginLeft: 10}}>
      	<div style={{fontSize: '1.25em', fontWeight: 'bold'}}>
        	{props.name}
        </div>
        <div>{props.company}</div>
      </div>
    </div>
  );
};
const CardList = (props) => {
	return (
  	<div>
    	{props.cards.map((card) => <Card {...card} />)}
    </div>
  );
}
class Form extends React.Component {
	handleSubmit = (e) => {
  	e.preventDefault()
    console.log('form submitted')
  }
	render() {
  	return (
    	<form onSubmit>
        <input type="text" placeholder="Github username" />
        <button type="submit">Add card</button>
      </form>
    )
  }
}
class App extends React.Component {
	state = {
  	cards: [{ name: "O'Rly",
  	avatar_url: "https://avatars.githubusercontent.com/u/8445?v=3",
    company: "Gotham Squares" },
	{ name: "Alpy",
  	avatar_url: "https://avatars.githubusercontent.com/u/6820?v=3",
    company: "Wearhouse" },
		]
	}
	render() {
  	return (
    	<div>
        <Form />
        <CardList cards={this.state.cards} />
      </div>
    )
  }
}
ReactDOM.render(<App />, mountNode);
```

**What to add to the state?** Any data that is to be modified.


#### Getting input from users
In **forms**, use the onSubmit in the form element instead of onClick in the submit button to handle. This allows use of the native form behaviors (e.g., required fields)

"Every React event function receives an event argument." All of the methods available on the native event object are likewise available for React handlers.

##### Uncontrolled element way to read input
To read input, just give it id and use getElementById(id) to read its value. React's `ref` property is used to create a reference to the element. It takes a function that will be executed when input element is mounted to DOM. The function takes the element which value can be stored on the "form component instance".

```javascript
<input
          ref=((input) => this.userNameInput = input)
          type="text"
          placeholder="Github username" />
```
##### Controlled element way to read input:
1. Add a userName property to state
2. Set value of input to be this property
3. set the onChange of input to update this value

```javascript
<input
          value={this.state.userName}
          onChange={(e) => this.setState({ userName: e.target.value })}
          type="text"
          placeholder="Github username" />
```

#### Fetch Data
Can use native fetch or axios, which comes with React.
```javascript
axios.get(`https://api.github.com/users/${this.state.userName}`)
    	.then(res => {
      	this.props.onSubmit(res.data)
        this.setState({ userName: '' })
      })
```


A component can't change the state of its parent, so when child component fetches data, it passes it to parent to handle:

1. create a method in App that updates state. This method takes in as an argument the data that will be be passed in from Form when it is called.
```javascript
addNewCard = (cardInfo) => {
  this.setState((prevState) => ({
      cards: prevState.cards.concat(cardInfo)
  }))
}
```
1. Pass this method to the Form class: `<Form onSubmit={this.addNewCard}`.
2. In the Form class create a function to handle the event. Pass the information into the method from App that will then handle the state change: `this.props.onSubmit(res.data)`.

*Et voila*.


##### Final code to search for, retrieve, and display users from gitub API:
```javascript
const Card = (props) => {
	return (
  	<div style={{margin: '1em'}}>
    	<img width="75" src={props.avatar_url} />
      <div style={{display: 'inline-block', marginLeft: 10}}>
      	<div style={{fontSize: '1.25em', fontWeight: 'bold'}}>
        	{props.name}
        </div>
        <div>{props.company}</div>
      </div>
    </div>
  );
};
const CardList = (props) => {
	return (
  	<div>
    	{props.cards.map((card) => <Card {...card} />)}
    </div>
  );
}
class Form extends React.Component {
	state = { userName: '' }
	handleSubmit = (e) => {
  	e.preventDefault()
    axios.get(`https://api.github.com/users/${this.state.userName}`)
    	.then(res => {
      	console.log('getting user')
      	this.props.onSubmit(res.data)
        this.setState({ userName: '' })
      })
  }
	render() {
  	return (
    	<form onSubmit={this.handleSubmit} >
        <input
          value={this.state.userName}
          onChange={(e) => this.setState({ userName: e.target.value })}
          type="text"
          placeholder="Github username" />
        <button type="submit">Add card</button>
      </form>
    )
  }
}
class App extends React.Component {
	state = {
  	cards: []
	}
  addNewCard = (cardInfo) => {
  	this.setState((prevState) => ({
    	cards: prevState.cards.concat(cardInfo)
    }))
  }
	render() {
  	return (
    	<div>
        <Form onSubmit={this.addNewCard} />
        <CardList cards={this.state.cards} />
      </div>
    )
  }
}
ReactDOM.render(<App />, mountNode);
```
