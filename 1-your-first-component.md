#### Notes on: Getting Started with React
#### [Pluralsight with Samer Buna](https://app.pluralsight.com/player?course=react-js-getting-started&author=samer-buna&name=react-js-getting-started-m1&clip=0&mode=live)
# 1: Your first component

[playground](https://jscomplete.com/repl)

**Components** are:
* Like functions
* reusable
* can manage _private_ state

React generates HTML by writing JavaScript while Angular enhances HTML directly. This allows React to have a virtual view in memory (the virtual DOM) which it can use for **tree reconciliation** to refresh only the changes to a view and not whole view when an event listener is triggered.

Components:
* Two types: class components and function components
* classes have stateses
* **internal state can change but props are fixed value**
* receive one argument: **props**
* have to be capitalized (to diff from reg HTML elements)

## Function component
Instead of hard-coding button label, we pass it as props. To pass props, it comes in as an argument, is used inside JSX, and is defined in the render part. **Props** is an object that contains all the values passed when the component is rendered.

```javascript
const Button = (props) => {
     return (
         <button>{props.label}</button>
     );
}
ReactDOM.render(<Button label="Do" />, mountNode);
```
**If the component does not change state, that's it. Nothing else.**

## Class component
If the component *does* need to change its own values, use a **class**.  Use the constructor function to declare the initial state. "Call the super function to honor the inheritance." "The 'this' keyword refers to the component instance we're handing off to ReactDON here."
```javascript
class Button extends React.Component {
     constructor(props) {
         super(props); //
         this.state = { counter: 0 }
     }
     render() {
         return (
              <button>{this.state.counter}</button>
         );
     }
}
ReactDOM.render(<Button />, mountNode);
```
ES6 provides a class property that simplifies the syntax, like so:
```javascript
class Button extends React.Component {
     state = { counter: 0 }

     render() {
         return (
              <button>{this.state.counter}</button>
         );
     }
}
ReactDOM.render(<Button />, mountNode);
```
React event handlers take a function whereas regular event handlers take a string (like 'click', or 'mouseover').

Standard practice is to define this function in the class component itself "as an instance property."

Then inside the render function, it can be referenced by the `this.handleClick` syntax.

These arrow functions inside the class are "bound to the component instance." The handleClick function will act as a prototype function on this class. Inside handleClick, 'this' refers to the component instance that will be sent to the DOM.

```javascript
class Button extends React.Component {
     state = { counter: 0 }
     handleClick = () => {
         this.setState({
              counter: 5
         })
     }
     render() {
         return (
              <button onClick={this.handleClick}>
                {this.state.counter}
              </button>
         )
     }
}
ReactDOM.render(<Button />, mountNode)
```
To change state in React:
1. It must be a class component
2. It must initiate with `state = {}` (ES6) or calling `constructor(props) {super(); this.state = {}}`
3. There must be a function defined in the class that calls React's setState method to update state
4. there must be something in the rendered JSX that triggers the change and calls the function

The `setState()` function is *async*, so if it needs to first read the previous state before updating, then instead of just passing in the to-be-updated object itself, pass in a callback that *returns* the object to be updated. This callback accepts the previous state as an argument. This better controls the event flow and avoids possible errors. (bit.ly/psreact1)

```javascript
class Button extends React.Component {
     state = { counter: 0 }
     handleClick = () => {
         this.setState((prevState) => ({
             counter: prevState.counter + 1
         }))
     }
     render() {
         return (
              <button onClick={this.handleClick}>
                {this.state.counter}
              </button>
         )
     }
}
ReactDOM.render(<Button />, mountNode)
```
