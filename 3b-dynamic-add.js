/*
A button to increment the displayed number by a user-input value.
Demonstrates data flowing from child class to parent class through closure.
*/
const Count = (props) => {
	return (
  	<div>
      {props.counter}
    </div>
  )
}

class Form extends React.Component {
	state = { inputVal: 0 }
	handleInputChange = (e) => {
  	this.setState({
    	inputVal: e.target.value
    })
  }
  handleSubmit = (e) => {
  	e.preventDefault()
    this.props.onSubmit(e.target[1].value)
  }
  handleClear = () => {
  	this.setState({
    	inputVal: 0
    })
    this.props.onClear()
  }

  render() {
  	return (
    	<form onSubmit={this.handleSubmit} >
        <button type="submit">
          Add {this.state.inputVal}
        </button><br />
        <input
          type="number"
          value={this.state.inputVal}
          onChange={this.handleInputChange}
          placeholder={0}
          min={0} >
        </input>
        <button onClick={this.handleClear} >
          Clear
        </button>
      </form>
    )
  }
}

class App extends React.Component {
	state = { counter: 0 }
  incrementBy = (num) => {
  	this.setState((prevState) => ({counter: Number(prevState.counter) + Number(num)
    }))
  }
  clearTotal = (count) => {
  	this.setState({
    	counter: 0
    })
  }
  render() {
  	return (
      <div>
        <Form
          onSubmit={this.incrementBy}
          onClear={this.clearTotal} />
        <Count counter={this.state.counter} />
      </div>
    )
  }
}

ReactDOM.render(< App />, mountNode)
