#### Notes on: Getting Started with React
#### [Pluralsight with Samer Buna](https://app.pluralsight.com/player?course=react-js-getting-started&author=samer-buna&name=react-js-getting-started-m1&clip=0&mode=live)
# 2: Reusable Components

[playground](https://jscomplete.com/repl)

[code](bit.ly/psreact2)

Since a React component can only return *one* element, make a single master component that returns a div that contains all other elements.

**The state of a component can only be accessed by the component itself.**

So if we want a button in one component to change the display contained in another component, the state and state change must be handled by the parent component. The parent can then pass the reference to this function or reference to result values to its various children.
```javascript
class Button extends React.Component {
     render() {
         return (
             <button onClick={this.props.onClickFunc}>
                 Count up by 1!
             </button>
         )
     }
}
const Result = (props) => {
    return (
      <div>{props.counter}</div>
  )
}
class App extends React.Component {
    state = { counter: 0 }

  incrementCounter = () => {
       this.setState((prevState) => ({
           counter: prevState.counter + 1
       }))
  }
    render() {
      return (
        <div>
        <Button onClickFunc={this.incrementCounter}/>
        <Result counter={this.state.counter}/>
      </div>
    )
  }
}
ReactDOM.render(<App />, mountNode)
```
Note how props are passed:
* as values to function components
* as states to class components

Syntax for this example:

|                     | child function component       | child class component                 |
|---------------------|--------------------------------|---------------------------------------|
| parent passes prop  | `counter={this.state.counter}` | `onClickFunc={this.incrementCounter}` |
| child accesses prop | `props.counter`                | `this.props.onClickFunc`               |

Ok, now to make the button reusable by abstracting out the increment value. Instead of hard-coding the 1, we pass in the value from the parent class.

We also need to abstract out the increment value from the handler function in the parent class. Ok, yeah, but this argument can't be passed directly to the class child. So... you get this monstrosity:

Note:
```javascript
<button>
    onClick= {() => this.props.onClickFunc(this.props.countVal)}>
</button>
```
>(How I understand this: The button property, onClick, is a React event handler. Here it is bound to a function that returns a function passed to it from its parent, which takes in a value also passed from its parent. In javascript, you can pass a function around, but no arguments can be added until it is called. So a closure is needed here to encapsulate the function. Somehow the function isn't immediately called when it's stuck inside another function, but it *is* immediately called when just inside an object (?!!?!?).)

Can also use bind. However both of these methods create a *new* function at every instance. So, instead of wrapping in an anonymous function, make a named handler in the child class that wraps it. Then just reference that in the onClick event. Apparently this is better.

```javascript
class Button extends React.Component {
    handleClick = () => {
        this.props.onClickFunc(this.props.countVal)
    }
     render() {
         return (
            <button
                onClick={this.handleClick} >
                Count up by {this.props.countVal}
            </button>
         )
     }
}
```
