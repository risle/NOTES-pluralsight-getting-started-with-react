#### Notes on: Getting Started with React
#### [Pluralsight with Samer Buna](https://app.pluralsight.com/player?course=react-js-getting-started&author=samer-buna&name=react-js-getting-started-m1&clip=0&mode=live)
# 4: Building the Game Interface


It can be useful to have to instances of child components displayed to ensure that state is working correctly (each should be a unique instance with different states).
